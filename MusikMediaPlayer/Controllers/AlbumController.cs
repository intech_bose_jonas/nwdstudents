﻿using Nwd.BackOffice.Impl;
using Nwd.BackOffice.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MusikMediaPlayer.Controllers
{
    public class AlbumController : Controller
    {
        [SuperAuthorize]
        [HttpGet]
        public ActionResult List()
        {
            var listAlbum = new AlbumRepository().GetAllAlbums();

            return View(listAlbum);
        }

        [SuperAuthorize]
        [HttpGet]
        public ActionResult Create()
        {
            var album = new Album { Artist = new Artist() };
            ViewBag.TrackNumber = album.Tracks.Count;
            return View(album);
        }

        [SuperAuthorize]
        [HttpPost]
        public ActionResult Create(Album album)
        {
            if (!IsValidAlbum(album))
                return View(album);

            var createdAlbum = new AlbumRepository().CreateAlbum(album, this.Server);
            return RedirectToAction("Details", new { createdAlbum.Id });
        }

        [SuperAuthorize]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var album = new AlbumRepository().GetAlbumForEdit(id);
            ViewBag.TrackNumber = album.Tracks.Count;
            return View(album);
        }

        [SuperAuthorize]
        [HttpPost]
        public ActionResult Edit(Album album)
        {
            if(!IsValidAlbum(album))
                return View(album);

            new AlbumRepository().EditAlbum(this.Server, album);
            return RedirectToAction("Details", new { album.Id });
        }

        [Authorize]
        [HttpGet]
        public ActionResult Details(int id)
        {
            var album = new AlbumRepository().GetAlbumForEdit(id);
          
            return View(album);
        }

        [Authorize]
        [HttpGet]
        public ActionResult Delete(int id)
        {
            new AlbumRepository().DeleteAlbum(id);

            return RedirectToAction("List");
        }

        [Authorize]
        public PartialViewResult BlankEditorTrack(int index)
        {
            ViewBag.TrackNumber = index;
            return PartialView("_CreateTrack", new Track());
        }

        private bool IsValidAlbum(Album album)
        {
            if (album.CoverImagePath == null)
            {
                if (!(album.CoverFile.ContentLength > 0))
                {
                    ViewBag.Error = "L'image n'éxiste pas";
                    return false;
                }
            }

            if (album.Tracks == null)
                return true;

            foreach (var track in album.Tracks)
            {
                if (track.FileRelativePath != null)
                    continue;

                if (!(track.File.ContentLength > 0))
                {
                    ViewBag.Error = "Un track n'éxiste pas";
                    return false;
                }
            }

            return true;
        }
	}
}