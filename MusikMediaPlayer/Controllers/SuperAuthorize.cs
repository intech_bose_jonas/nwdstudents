﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MusikMediaPlayer.Controllers
{
    public class SuperAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!httpContext.Request.IsAuthenticated || httpContext.User.Identity.Name != "SuperAdmin")
                return false;

            return true;
        }
    }
}
